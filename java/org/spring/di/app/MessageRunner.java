package org.spring.di.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.di.xml.ConstructorMessage;
import org.spring.di.xml.DefaultMessage;
import org.spring.di.xml.SetterMessage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Message application.
 * 
 * @author Rajive Gayan
 */
public class MessageRunner {

    final static Logger logger = LoggerFactory.getLogger(MessageRunner.class);
    
    /**
     * Main method.
     */
    public static void main(String[] args) {
    	
        logger.info(" ==================== Initializing Spring context. ==================== ");
        
        //ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/application-context.xml", "mnmnm");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/application-context.xml");
        
        logger.info(" ==================== Spring context initialized. ==================== ");
        
        DefaultMessage messageDefault = (DefaultMessage) applicationContext.getBean("messageDefault");
        logger.debug("--------------------------- DefMessage='" + messageDefault.getMessage() + "'");
        //System.out.println("--------------------------- DefMessage='" + messageDefault.getMessage() + "'");

        SetterMessage message = (SetterMessage) applicationContext.getBean("message");
        logger.debug("++++++++++++++++++++++++ SetMessage='" + message.getMessage() + "'");
        //System.out.println("++++++++++++++++++++++++ SetMessage='" + message.getMessage() + "'");
        
        ConstructorMessage constructorMessage = (ConstructorMessage)applicationContext.getBean("messageCon");
        logger.debug("++++++++++++++++++++++++ ConMessage='" + constructorMessage.getMessage() + "'");
        //System.out.println("++++++++++++++++++++++++ ConMessage='" + constructorMessage.getMessage() + "'");
    }

}
