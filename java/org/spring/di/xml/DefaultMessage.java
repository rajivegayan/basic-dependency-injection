package org.spring.di.xml;

/**
 * Default message bean.
 * 
 * @author Rajive Gayan
 */
public class DefaultMessage {

    private String message = "Spring Default Message";

    /**
     * Gets message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
