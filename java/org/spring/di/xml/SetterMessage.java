package org.spring.di.xml;

/**
 * Message bean.
 * 
 * @author Rajive Gayan
 */
public class SetterMessage {

    private String message = null;

    /**
     * Gets message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
