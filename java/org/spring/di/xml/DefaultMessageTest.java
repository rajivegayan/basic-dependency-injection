package org.spring.di.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spring.di.xml.DefaultMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DefaultMessageTest {

    final Logger logger = LoggerFactory.getLogger(DefaultMessageTest.class);

    @Autowired
    private DefaultMessage message = null;

    /**
     * Tests message.
     */
    @Test
    public void testMessage() {   
        assertNotNull("Default message instance is null.", message);
        
        String msg = message.getMessage();
        
        assertNotNull("Message is null.", msg);
        
        String unexpectedMessage = "Spring is fun.";
        String expectedMessage = "Spring Default Message";
        
        assertEquals("Message should be '" + expectedMessage + "'.", unexpectedMessage, msg);

        logger.info("message='{}'", msg);
    }
    
}
